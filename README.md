[![pipeline status](https://gitlab.com/eisoku9618/report/badges/master/pipeline.svg)](https://gitlab.com/eisoku9618/report/commits/master)
[![coverage report](https://gitlab.com/eisoku9618/report/badges/master/coverage.svg)](https://gitlab.com/eisoku9618/report/commits/master)

## 成果物
- https://gitlab.com/eisoku9618/report/-/jobs/artifacts/master/browse?job=latex_job

## 使い方

### 準備

```bash
docker pull eisoku9618/latex:latex-japanese
```

### tex -> pdfをtexが更新される度に行う
```bash
docker run --rm -it --user=`id -u $USER` -w=/root -v $(pwd)/example:/root eisoku9618/latex:latex-japanese /bin/bash -c "latexmk -pvc main.tex"
```
でOK．
表示するには
```bash
evince example/.tmp/main.pdf
```

## memo

もともと https://github.com/eisoku9618/report でやろうとしていたことの2018年バージョン．
dockerを使ったりして進化させた．
